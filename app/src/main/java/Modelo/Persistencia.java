package Modelo;

import com.example.preexamenc2java.Usuarios;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuarios usuario);
}
